{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 0: Gravitational-wave strain\n",
    "\n",
    "From the exercise we did in lecture, write a Python script that calculates the gravitational-wave strain $h$ for the following systems:\n",
    "1. $m_1=1.46 M_\\odot, m_2=1.27M_\\odot$, $D = 40$ Mpc\n",
    "2. $m_1=13.7 M_\\odot, m_2=7.7 M_\\odot$, $D= 440$ Mpc\n",
    "3. $m_1=50.6 M_\\odot, m_2=34.3 M_\\odot$, $D= 2750$ Mpc\n",
    "\n",
    "Note: To ease ourselves into using LIGO's data analysis software, import `lal` and import the fundamental constants:\n",
    "\n",
    "```python\n",
    "import lal\n",
    "from lal import PC_SI, PI, MSUN_SI, G_SI, C_SI # We will need these constants to convert to SI\n",
    "#PC_SI: Converts parsecs to metres\n",
    "#PI: pi\n",
    "#MSUN_SI: Converts solar mass to kilograms\n",
    "#G_SI: Gravitational constant in SI units\n",
    "#C_SI: speed of light in SI units```\n",
    "\n",
    "For future reference, see https://lscsoft.docs.ligo.org/lalsuite/lal/group___l_a_l_constants__h.html for more constants provided by `lal`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 1: Detectable inspiral range (8 questions)\n",
    "\n",
    "To determine a detector's sensitivity in terms of the sources it can observe, it's useful to estimate its <b>detectable inspiral range</b>. In this exercise, we will compute the detectibility range for binary neutron stars at Advanced LIGO's design sensitivity.\n",
    "\n",
    "First, let us consider a binary neutron star (BNS) system. It is composed of two non-spinning neutron stars, each of mass 1.4$M_\\odot$, which are orbiting in a circular orbit. Answer the following (using Newtonian physics):\n",
    "\n",
    "1. When the orbital period of this system is 0.1 s, what is the frequency of emitted gravitational waves (in Hz)?\n",
    "2. Find their radial separation $r$ (in km), their velocity (in km/s), and $v/c$.\n",
    "3. The innermost stable circular orbit (ISCO) is defined as the minimum radial distance beyond which stable circular orbits are no longer allowed. When ISCO is approached, dynamics are dominated by strong-field effects, which causes the two stars to plunge towards each other. Determine the ISCO of the system, where $r_{ISCO}=6GM/c^2$. Give your answer in kilometres.\n",
    "4. What is the orbital period and velocity of the system at ISCO? What is the gravitational-wave frequency?\n",
    "\n",
    "From general relativity, we can compute the strain $h$ in the frequency domain, using the quadrupole approximation. This is given as\n",
    "\n",
    "$$\\tilde{h}(f) = \\frac{1}{\\pi^{2/3}}\\Big(\\frac{5}{24}\\Big)^{1/2} \\frac{c}{D}\\Big(\\frac{G\\mathcal{M}_c}{c^3}\\Big)^{5/6}\\frac{1}{f^{7/6}}e^{-i\\Phi(f)} \\\\\n",
    "\\Phi(f) = 2\\pi f t_c - 2\\Phi_c - \\frac{\\pi}{4} + \\frac{3}{4}\\Big(\\frac{G\\mathcal{M}_c}{c^3} 8\\pi f\\Big)^{-5/3}$$\n",
    "\n",
    "where $\\mathcal{M}_c = \\mu^{3/5} M^{2/5}$ is the chirp mass (where $\\mu = m_1 m_2/M$ and $M=m_1+m_2$), $D$ is the luminosity distance, $t_c$ is the time at coalescence, $\\Phi_c$ is the phase $\\Phi$ at coalescence (Maggiore, 2008).\n",
    "\n",
    "The <b>signal-to-noise ratio</b> $\\rho$ of a signal $h(t)$ found in data $d(t)$ is given as\n",
    "\n",
    "$$\\rho^2 = \\langle d, h \\rangle = 4\\int_{f_{\\mathrm{min}}}^{f_{\\mathrm{max}}} \\frac{\\tilde{d}(f)\\tilde{h}^*(f)}{S_n(f)} df,$$\n",
    "\n",
    "The term $\\langle a,b\\rangle$ is called the <b>noise-weighted inner product</b>, as it is weighted by the power spectral density (PSD) $S_n(f)$. At design sensitivity, the\"zero-detuning, high power\" (zero-detuning of the signal recycling mirror with full laser power) PSD of Advanced LIGO at design sensitivity is expected to follow the following analytic fit:\n",
    "\n",
    "$$S_n(f) = 10^{-48} ( 0.0152 x^{-4} + 0.2935 x^{9/4} + 2.7951 x^{3/2} - 6.5080 x^{3/4} + 17.7622)$$\n",
    "\n",
    "where $x = f/245.4$ (arXiv:1107.1267). To get some more practice with `lal`, let's generate the PSD object by the following:\n",
    "\n",
    "```python\n",
    "import lal\n",
    "\n",
    "freq = np.arange(0, f_high+deltaf, deltaf)\n",
    "x = freq/245.4\n",
    "analytic_psd = # input analytic S_n(f) equation\n",
    "psd = lal.CreateREAL8FrequencySeries(\"PSD\", lal.LIGOTimeGPS(0.), 0., deltaf, lal.HertzUnit, len(analytic_psd)) # the unit given in the 5th argument is wrong, but we just need to input a unit there\n",
    "psd.data.data = analytic_psd```\n",
    "\n",
    "5. Plot the amplitude spectral density (ASD = $\\sqrt{S_n(f)}$) on a log-log scale, setting $f_{\\mathrm{min}} = 15$ Hz and $f_{\\mathrm{max}} = 5000$ Hz. Plot the $h(f)$ spectrum on top of the ASD, making sure to correct the unit mismatch. Assume the system is optically oriented to the detector (such that the exponential phase term is 1). Choose distance $D=1$ Mpc.\n",
    "6. Numerically integrate to find the SNR of the BNS inspiral signal at a distance of $D = 1$ Mpc. For this question, in the absence of data, we want to compute the <b>optimal signal-to-noise ratio</b>, which is $\\langle h, h \\rangle$.\n",
    "7. At what distance (in Mpc) can we see such a signal with SNR of 8? This distance is called the BNS horizon distance.\n",
    "8. Repeat Q1-Q7 for a binary black hole (BBH) system ($m_1=m_2 = 10M_\\odot$) and a neutron star black hole (NSBH) system ($m_1 = 10M_\\odot, m_2=1.4M_\\odot$)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 2: Waveform templates (6 questions)\n",
    "\n",
    "In this exercise, we will use tools from the LIGO Analysis Library Suite (LALSuite) to generate waveforms from semi-analytic waveform models. To create a <b>time-domain waveform</b>, we will use the modules `lalsimulation` and `lal`:\n",
    "\n",
    "```python\n",
    "import lalsimulation as lalsim\n",
    "\n",
    "param = lal.CreateDict()\n",
    "approximant = lalsim.GetApproximantFromString('SEOBNRv4_opt') # Specify the waveform model to be SEOBNRv4\n",
    "hplus, hcross = lalsim.SimInspiralTD(mass1_in_SI_units, mass2_in_SI_units, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z, distance, PI/3, 0.0, PI/2, 0.0, 0.0, delta_t, f_low, f_ref, param, approximant)```\n",
    "\n",
    "Similarly, to generate a <b>frequency-domain waveform</b>:\n",
    "\n",
    "```python\n",
    "approximant = lalsim.GetApproximantFromString('SEOBNRv3_ROM') # Different waveform model for frequency domain\n",
    "hplus, hcross = lalsim.SimInspiralFD(mass1_in_SI_units, mass2_in_SI_units, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z, distance, PI/3, 0.0, PI/2, 0.0, 0.0, delta_f, f_low, f_high, f_ref, param, approximant)```\n",
    "\n",
    "Note the differences in waveform approximation and input arguments. The documentation for these functions can be found here: https://git.ligo.org/lscsoft/lalsuite/blob/master/lalsimulation/src/LALSimInspiral.h.\n",
    "\n",
    "1. To get some practice generating waveforms, we'll use the GW detection catalogue (https://www.gw-openscience.org/catalog/GWTC-1-confident/html/). Make a scatter plot of the mass parameters $m_1$ and $m_2$ of each event (11 in total). For the waveform that looks like GW170817, use the approximant `TaylorT2`, instead of the EOB waveform model.\n",
    "2. Compare the parameter space of these events with LIGO's first observing (O1) run template bank. You can load in the file `O1_template_bank.xml.gz` and extract its parameters by the following:\n",
    "\n",
    "```python\n",
    "from glue.ligolw import ligolw, utils, lsctables\n",
    "lsctables.use_in(ligolw.LIGOLWContentHandler)\n",
    "\n",
    "xmldoc = utils.load_filename('O1_template_bank.xml.gz', contenthandler=ligolw.LIGOLWContentHandler)\n",
    "O1_template_bank = lsctables.SnglInspiralTable.get_table(xmldoc)\n",
    "O1_params = []\n",
    "\n",
    "for tmplt in O1_template_bank:\n",
    "    O1_params.append([tmplt.mass1, tmplt.mass2, tmplt.spin1z, tmplt.spin1y, tmplt.spin1z, tmplt.spin2x, tmplt.spin2y, tmplt.spin2z])\n",
    "    \n",
    "O1_params = np.array(O1_params)```\n",
    "\n",
    "How many templates does the O1 template bank have? What are the mass ranges for $m_1$ and $m_2$? The spin ranges? To your plot, add dashed lines that denote constant chirp mass $\\mathcal{M}_c=\\{1, 2, 5, 10, 20\\} M_\\odot$.\n",
    "3. Now, with the function `lalsimulation.SimInspiralTD`, generate and plot time domain waveforms of your event-based template bank, using the SEOBNRv4_opt waveform model and taking all spins to be zero. Notice the length of the GW170817-like waveform compared to the others. \n",
    "\n",
    "Because the GW170817-like waveform is so different from the others, we're going to exclude it for simplicity and call the remaining 10 waveforms our 'template bank' - it's a really bad template bank, and we'll see why shortly. In matched-filtering, we normalize the templates (such that $|h|=1$) by weighting it by\n",
    "\n",
    "$$\\sigma^2 = 4 \\int_{f_{\\mathrm{min}}}^{f_{\\mathrm{max}}} \\frac{|\\tilde{h}(f)|^2}{S_n(f)}df $$.\n",
    "\n",
    "Another useful quantity to compute is the <b>overlap</b> between templates $h_1$ and $h_2$ (both normalized by $h/\\sigma^2$). The overlap informs us how well the templates match, and is given as\n",
    "\n",
    "$$ \\mathcal{O} = \\max_{t_0, \\phi_0}\\frac{\\langle h_1(\\phi_0, t_0), h_2\\rangle}{\\sqrt{\\langle h_1, h_1\\rangle \\langle h_2, h_2 \\rangle}}$$\n",
    "\n",
    "where $\\langle \\cdot, \\cdot \\rangle$ denotes the noise-weighted inner product. $t_0$ and $\\phi_0$ are the time and phase shift, respectively, that yield the largest overlap between $h_1$ and $h_2$. Since the templates are normalized, $\\mathcal{O} = 1$ indicate the waveforms are identical, and decreases as the waveforms become less well-matched. In LIGO's matched-filter searches, waveform templates are specifically placed such that SNR loss arising from the discreteness of the template bank does not exceed $x=10$%. This corresponds to a minimum overlap requirement of $\\sqrt[3]{1-x}\\approx 0.97$ for normalized templates that are adjacent to each other in waveform space.\n",
    "\n",
    "The overlaps are computed in the frequency-domain to calculate the SNR integrand, and then reverse Fourier transformed to obtain the complex overlap timeseries. The maximum overlap is thus the overlap maximized over time and phase. Some example code is provided below.\n",
    "\n",
    "```python\n",
    "len1side = int(fNyq/deltaF)+1 # length of Hermitian arrays\n",
    "len2side = 2*(len1side-1) # length of non-Hermitian arrays\n",
    "deltaT = 1./deltaF/len2side\n",
    "revplan = lal.CreateReverseCOMPLEX16FFTPlan(len2side, 0) # Set up reverse FFT plan\n",
    "# Create empty series, to be populated later\n",
    "intgd = lal.CreateCOMPLEX16FrequencySeries(\"SNR integrand\", lal.LIGOTimeGPS(0.), 0., deltaF, lal.HertzUnit, len2side)\n",
    "ovlp = lal.CreateCOMPLEX16TimeSeries(\"Complex overlap\", lal.LIGOTimeGPS(0.), 0., deltaT, lal.DimensionlessUnit, len2side)\n",
    "    \n",
    "def overlap(h1, h2):\n",
    "    intgd.data.data[:] = 0.0 + 0.0j\n",
    "    temp = 4.*np.conj(h1.data.data)*h2.data.data*(1./psd.data.data)\n",
    "    intgd.data.data[len1side-1:] = temp[:-1]\n",
    "    lal.COMPLEX16FreqTimeFFT(ovlp, intgd, revplan)\n",
    "    o = np.abs(ovlp.data.data)\n",
    "    return o.max()```\n",
    "\n",
    "4. With the function `lalsimulation.SimInspiralFD`, generate frequency-domain waveforms of the same events; use the SEOBNRv4_ROM waveform model (and again, take spins = 0). Normalize the frequency-domain waveforms, then use them to compute the overlaps of our template bank (the example code above might help). Check that the overlaps are a 10x10 symmetric matrix and that $\\langle h_1, h_1\\rangle = 1$. \n",
    "5. For each template $h_1$, identify $h_2$, the waveform that best matches it. You'll find that for our template bank, $h_2$ will be the closest template to $h_1$ in terms of chirp mass. Compute the SNR loss for the template and its best-matched template. Compare this against LIGO's minimum SNR loss of 10%. \n",
    "6. Referring the plot you made in Q2 for LIGO's O1 template bank, compare the density of templates in the low mass regime versus high mass regime. Knowing that the minimum overlap between adjacent templates is 0.97 (don't prove this), why isn't the template bank density uniform over mass space? (Hint: think about template duration.) For templates the same chirp mass, do you expect their overlaps to be large or small? Why? (Hint: Recall the strain equation.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 3: Basic match-filtering (4 questions)\n",
    "\n",
    "Now, we are finally going to analyze some data. \n",
    "\n",
    "From https://www.gw-openscience.org/catalog/GWTC-1-confident/html/, click on the event GW150914 and download the 32-second, 16384 Hz sampling rate HDF5 files for both Hanford (H1) and Livingston (L1) detectors.\n",
    "\n",
    "Here's how to read the HDF5 file:\n",
    "\n",
    "```python\n",
    "import h5py\n",
    "f = h5py.File('H-H1_GWOSC_16KHZ_R1-1126259447-32.hdf5', 'r')\n",
    "f.keys() # show attributes\n",
    ">>> [u'meta', u'quality', u'strain']\n",
    "f['strain'].keys()\n",
    ">>> ['Strain']\n",
    "f['strain']['Strain'].value\n",
    ">>> array([  6.09513456e-21,   1.48369999e-20,   2.28723129e-20, ...,\n",
    "         6.18140599e-20,   1.01283600e-19,   1.20014866e-19])\n",
    "```\n",
    "\n",
    "The attribute 'meta' also contains useful information, such as the GPS start time and duration. The attribute 'quality' contains information regarding the quality of the data that was taken.\n",
    "\n",
    "1. For each detector, calculate its corresponding PSD from the strain data. You can use the function `matplotlib.mlab.psd` (https://matplotlib.org/api/mlab_api.html, set NFFT = 4\\*Fs) to find the PSD. Plot the ASD of both detectors, along with the design sensitivity ASD from Exercise 1. What is the Nyquist frequency?\n",
    "2. To determine the sensitivity of the detectors at the time of the signal, calculate the BNS inspiral range for Hanford and Livingston (recall the canonical BNS system is $1.4 + 1.4 M_\\odot$ system at $\\rho=8$). The calculation will be similar as in Exercise 1, except you will use the PSD calculated from strain data.\n",
    "4. Write a script to perform match-filtering to find a signal in the data. The script should achieve the following (the functions found under `np.fft` may be useful).:\n",
    "    1. Load in the strain data for both detectors.\n",
    "    2. Compute a PSD window to reduce spectral leakage using `np.blackman`, choosing NFFT = 4\\*Fs. Using the PSD window, compute the PSD using `matplotlib.mlab.psd`, choosing NFFT = 4\\*Fs and noverlap = NFFT/2.\n",
    "    3. Window the data and template using the Tukey window `scipy.signal.tukey`, choosing alpha=0.125 (why do we window the data and template?). Calculate the matched-filter output by multiplying the Fourier space data and template and then dividing by the PSD in each frequency bin.\n",
    "    4. Take the inverse Fourier transform of the matched-filter output to put it back in the time domain. Normalize the output so that a value of 1 is expected at times of pure noise.\n",
    "    5. Shift the SNR vector by the location of the template's peak so the peak is at the end of the template. Determine the time and SNR at maximum. Compute the network SNR $\\rho_c = \\sqrt{\\rho_{H1}^2 + \\rho_{L1}^2}$ of the signal at maximum.\n",
    "    6. Plot the SNR timeseries for each detector.\n",
    "    7. Repeat for all templates in the template bank. Devise and implement a strategy to determine when a signal has been found, and have your script identify the waveform template that best matches the data. Think about coincidence, signal strength, etc.\n",
    "4. Repeat Q1-3 using the strain data for GW170608.\n",
    "\n",
    "<b>Note:</b> This exercise is meant only to demonstrate the basic technique of matched-filtering using LIGO data. LIGO's detection pipelines employ much more sophisticated techniques than what we will use here; in particular, a crucial aspect of gravitational-wave detection is in the computation of false-alarm rates and ranking statistics in order to accurately differentiate astrophysical signals from noise. We'll discuss these techniques in further detail during the lectures."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 4: Markov Chain Monte Carlo methods\n",
    "\n",
    "To be added (maybe)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
