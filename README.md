# README #

## To activate environment: ##

```
source /home/heatherfong/env.sh
source /home/heatherfong/venv/lal/bin/activate
```

## To open Jupyter notebook: ##

### on NCU: ###

```
cd ncu_gwda
jupyter notebook --no-browser --port=X
```
where X is a 4-digit port number that can be chosen. When the above command is run, it will print a URL that looks something like `http://localhost:Y/?token...` where Y may be a different value from X.

### on local computer: ###

```
ssh -N -f -L localhost:Y:localhost:Y username@cms01.phy.ncu.edu.tw
```
where Y is the 4-digit number obtained previously. Use your own username.

Copy and paste the URL into your web browser.